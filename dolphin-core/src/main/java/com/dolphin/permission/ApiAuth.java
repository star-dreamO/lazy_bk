package com.dolphin.permission;


import com.dolphin.model.Permission;

import java.lang.annotation.*;


@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented

public @interface ApiAuth {


    /**
     * 权限标识，类似 shiro
     * 推荐规则：[aaa]:[bbb]:[ccc]
     * aaa->填写某个模块的标识
     * bbb->填写模块下某个功能的标识
     * ccc->填写某个功能的某种操作的标识
     * eg：例如管理员模块下的用户模块的添加操作-> management:user:create
     *
     * @return 该资源的权限标识，唯一
     */

    String permission() default "";

    /**
     * 该权限标识的意义
     *
     * @return 说明
     */
    String name() default "";


    /**
     * 所属菜单
     */
    String groupId() default "";

    /**
     * url的类型
     * 可做导航或者其他两种类型
     *
     * @return
     */
    Permission.ResType type() default Permission.ResType.OTHER;

}
