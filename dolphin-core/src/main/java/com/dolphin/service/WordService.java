package com.dolphin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dolphin.commons.Pager;
import com.dolphin.model.Word;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author dohinp
 * @create 2022-10-24 22:23
 * 个人博客地址：https://www.nonelonely.com
 */
public interface WordService extends IService<Word> {
    /*/**
     * @title
     * @description [pager]
     * @author dohinp
     * @updateTime 2022/10/24 22:26
     * @throws
     */
    Pager<Word> list(Pager<Word> pager);

    /*/**
     * @title
     * @description [pager]
     * @author dohinp
     * @updateTime 2022/10/24 22:26
     * @throws
     */
    Word getWordRandom(int limit);
}
