package com.dolphin.service;

/**
 * @description rss生成
 * @author dolphin
 * @date 2021/11/24 13:58
 */
public interface RssServices {
    /**
     * @description 获取rss信息
     * @return java.lang.String
     * @author dolphin
     */
    String genRss();
}
