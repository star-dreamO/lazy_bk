package com.dolphin.service;

import com.dolphin.model.Comment;
import com.dolphin.model.User;

/**
 * @description MailService
 * @author dolphin
 * @date 2021/11/15 10:20
 */
public interface MailService {

    /**
     * @description 发送邮件
     * @param  comment dolphin-comment
     * @author dolphin
     */
    void commentMailSend(Comment comment);

    /**
     * @description 密码
     * @author dolphin
     */
    void passwordMail(User user, String random) throws Exception;

    /**
     * 发送邮件
     */
    boolean sendMail(String mail,String content, String title) throws Exception;
}
