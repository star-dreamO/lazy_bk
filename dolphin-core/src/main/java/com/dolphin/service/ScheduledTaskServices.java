package com.dolphin.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.dolphin.commons.Pager;
import com.dolphin.model.ScheduledTask;

import java.util.List;

public interface ScheduledTaskServices extends IService<ScheduledTask> {

    Pager<ScheduledTask> list(Pager<ScheduledTask> pager);

    /*/**
     * @title 获取所有要启动的定时
     * @description []
     * @author 林平
     * @updateTime 2022/9/17 23:08
     * @throws
     */
    List<ScheduledTask> taskList();

    /**
     * 根据任务key 启动任务
     */
    Boolean start(String taskKey);

    /**
     * 根据任务key 停止任务
     */
    Boolean stop(String taskKey);

    /**
     * 根据任务key 重启任务
     */
    Boolean restart(String taskKey);


    /**
     * 程序启动时初始化  ==> 启动所有正常状态的任务
     */
    void initAllTask(List<ScheduledTask> scheduledTaskBeanList);

    /**
     * 根据任务key 获取对象
     */
    ScheduledTask getScheduledTaskByTaskKey(String taskKey);
}
