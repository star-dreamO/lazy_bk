package com.dolphin.service;

import com.dolphin.commons.Pager;
import com.dolphin.model.Photo;


public interface PhotoService {


    void addPhoto(Photo photo);

    Pager<Photo> list(Pager<Photo> pager);

    int del(String id);

    Photo getById(long id);

    int update(Photo photo);
}
