package com.dolphin.model.monitor;

import com.dolphin.commons.ArithUtil;

/**
 * 描述:內存相关信息
 * ${DESCRIPTION}
 *
 * @author nonelonely
 * @create 2020-12-07 0:33
 * 个人博客地址：https://www.nonelonely.com
 */
public class Mem {
    /**
     * 内存总量
     */
    private double total;

    /**
     * 已用内存
     */
    private double used;

    /**
     * 剩余内存
     */
    private double free;

    public double getTotal() {
        return ArithUtil.div(total, (1024 * 1024 * 1024), 2);
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public double getUsed() {
        return ArithUtil.div(used, (1024 * 1024 * 1024), 2);
    }

    public void setUsed(long used) {
        this.used = used;
    }

    public double getFree() {
        return ArithUtil.div(free, (1024 * 1024 * 1024), 2);
    }

    public void setFree(long free) {
        this.free = free;
    }

    public double getUsage() {
        return ArithUtil.mul(ArithUtil.div(used, total, 4), 100);
    }
}
