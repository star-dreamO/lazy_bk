package com.dolphin.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * option table
 * @author dolphin
 */
@ApiModel(value="Option-配置项数据",description="配置项数据")
@TableName("p_option")
public class Option implements Serializable {
    private static final long serialVersionUID = 7817277417501762377L;

    @ApiModelProperty(value="配置项ID",name="id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value="配置key",name="optionKey")
    private String optionKey;

    @ApiModelProperty(value="配置value",name="optionValue")
    private String optionValue;

    @ApiModelProperty(value="描述",name="remark")
    private String remark;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOptionKey() {
        return optionKey;
    }

    public void setOptionKey(String optionKey) {
        this.optionKey = optionKey;
    }

    public String getOptionValue() {
        return optionValue;
    }

    public void setOptionValue(String optionValue) {
        this.optionValue = optionValue;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
