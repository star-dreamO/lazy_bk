package com.dolphin.scheduled;

import com.dolphin.commons.ScheduledTaskAnnotation;
import com.dolphin.commons.WebSocketMsg;
import com.dolphin.component.desktop.DeskTopWebServer;
import com.dolphin.model.monitor.Server;
import com.dolphin.service.ScheduledTaskJob;
import com.qiniu.util.Json;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.Session;
import java.util.Map;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author 林平
 * @create 2022-09-18 13:06
 * 个人博客地址：https://www.nonelonely.com
 */
@ScheduledTaskAnnotation(initStartFlag = 1, taskDesc = "服务器监控", taskCron = "0/3 * * * * ? ")
public class MonitorJob implements ScheduledTaskJob {
    private final Logger log = LoggerFactory.getLogger(MonitorJob.class);

    @Override
    public void run() {
        try {
            Map<String, Session> deskTopWebServerSessionMap = DeskTopWebServer.deskTopWebServerSessionMap;
            if (deskTopWebServerSessionMap.size() > 0) {
                Server server = new Server();
                server.copyTo();
                for (Session session : deskTopWebServerSessionMap.values()) {
                    WebSocketMsg webSocketMsg = new WebSocketMsg();
                    webSocketMsg.setData(server);
                    webSocketMsg.setType(1);
                    session.getBasicRemote().sendText(Json.encode(webSocketMsg));
                }
            }
        } catch (Exception e) {
            log.error("MonitorJob执行失败{}", e);
        }
    }

}
