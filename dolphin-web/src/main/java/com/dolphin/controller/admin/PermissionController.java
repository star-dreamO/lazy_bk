package com.dolphin.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.dolphin.base.BaseController;
import com.dolphin.commons.Constants;
import com.dolphin.commons.Pager;
import com.dolphin.commons.ResponseBean;
import com.dolphin.model.Permission;
import com.dolphin.permission.ApiAuth;
import com.dolphin.service.PermissionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin")
public class PermissionController extends BaseController {
    private final Logger logger = LoggerFactory.getLogger(PermissionController.class);
    @Autowired
    private PermissionService permissionService;

    /**
     * 权限管理列表页
     *
     * @return String
     */
    @RequestMapping("/permission")
    @ApiAuth(name = "权限管理", permission = "admin:permission:index", groupId = Constants.ADMIN_MENU_GROUP_AUTH, type = Permission.ResType.NAV_LINK)
    public String index() {
        return view("static/admin/pages/permission/list.html");
    }

    /**
     * @param pager pager
     * @return
     * @description 角色分页
     * @author dolphin
     */
    @PostMapping("/permission/list")
    @ApiAuth(name = "权限管理列表数据", permission = "admin:permission:list")
    @ResponseBody
    public Pager<Permission> list(@RequestBody Pager<Permission> pager) {
        return permissionService.list(pager);
    }

    /**
     * 权限添加页
     *
     * @return String
     */
    @RequestMapping("/permission/addPage")
    @ApiAuth(name = "权限添加页", permission = "admin:permission:addPage", type = Permission.ResType.NAV_LINK)
    public String addPage(Model model) {
        return view("static/admin/pages/permission/add.html");
    }

    /**
     * 权限编辑页
     *
     * @return String
     */
    @RequestMapping("/permission/editPage/{id}")
    @ApiAuth(name = "权限编辑页", permission = "admin:permission:editPage", type = Permission.ResType.NAV_LINK)
    public String editPage(@PathVariable("id") String id, Model model) {
        Permission permission = permissionService.getById(id);
        model.addAttribute("model", permission);
        return view("static/admin/pages/permission/edit.html");
    }

    /**
     * 添加权限
     *
     * @return String
     */
    @PostMapping("/permission/add")
    @ResponseBody
    @ApiAuth(name = "添加权限", permission = "admin:permission:add")
    public ResponseBean add(@RequestBody @Valid Permission permission) {
        Permission permissionQuery = new Permission();
        permissionQuery.setUrl(permission.getUrl());
        QueryWrapper<Permission> queryWrapper = new QueryWrapper<>(permissionQuery);
        Permission menuByUrl = permissionService.getOne(queryWrapper);
        if (menuByUrl != null) {
            return ResponseBean.fail("权限链接已存在", null);
        }
        if (permissionService.save(permission)) {
            permissionService.updatePermission();
            return ResponseBean.success("添加成功", null);
        }
        logger.error("权限添加失败: {}", permission.toString());
        return ResponseBean.fail("添加失败", null);
    }

    /**
     * 更新权限
     *
     * @return String
     */
    @PostMapping("/permission/update")
    @ResponseBody
    @ApiAuth(name = "更新权限", permission = "admin:permission:update")
    public ResponseBean update(@RequestBody @Valid Permission permission) {
        Permission permissionQuery = new Permission();
        permissionQuery.setUrl(permission.getUrl());
        QueryWrapper<Permission> queryWrapper = new QueryWrapper<>(permissionQuery);
        Permission menuByUrl = permissionService.getOne(queryWrapper);
        if (menuByUrl != null && !menuByUrl.getId().equals(permission.getId())) {
            return ResponseBean.fail("权限链接已存在", null);
        }
        if (permissionService.saveOrUpdate(permission)) {
            permissionService.updatePermission();
            return ResponseBean.success("更新成功", null);
        }
        logger.error("权限更新失败: {}", permission);
        return ResponseBean.fail("更新失败", null);
    }

    /**
     * 删除权限
     *
     * @return String
     */
    @PostMapping("/permission/del")
    @ResponseBody
    @ApiAuth(name = "删除权限", permission = "admin:permission:del")
    public ResponseBean del(@RequestBody String ids) {
        if (permissionService.removeById(ids)) {
            permissionService.updatePermission();
            return ResponseBean.success("删除成功", null);
        }
        logger.error("权限删除失败: {}", ids);
        return ResponseBean.fail("删除失败", null);
    }

    /**
     * 更改状态
     *
     * @return String
     */
    @PostMapping("/permission/changeStatus")
    @ResponseBody
    @ApiAuth(name = "更改权限状态", permission = "admin:permission:changeStatus")
    public ResponseBean changeStatus(@RequestBody Permission permission) {
        if (permissionService.changeEnable(permission) > 0) {
            permissionService.updatePermission();
            return ResponseBean.success("修改成功", null);
        }
        logger.error("权限修改失败: {}", permission.toString());
        return ResponseBean.fail("修改失败", null);
    }

}
