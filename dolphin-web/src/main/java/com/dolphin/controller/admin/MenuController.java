package com.dolphin.controller.admin;

import com.dolphin.base.BaseController;
import com.dolphin.commons.Constants;
import com.dolphin.commons.ResponseBean;
import com.dolphin.commons.StringUtil;
import com.dolphin.model.Menu;
import com.dolphin.model.Permission;
import com.dolphin.permission.ApiAuth;
import com.dolphin.service.MenuService;
import com.dolphin.vo.RoleMenuVO;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class MenuController extends BaseController {
    private final Logger logger = LoggerFactory.getLogger(MenuController.class);
    @Autowired
    private MenuService menuService;

    /**
     * 菜单管理列表页
     *
     * @return String
     */
    @RequestMapping("/menu")
    @ApiAuth(name = "菜单管理", permission = "admin:menu:index", groupId = Constants.ADMIN_MENU_GROUP_AUTH, type = Permission.ResType.NAV_LINK)
    public String index() {
        return view("static/admin/pages/menu/menu_list.html");
    }

    /**
     * 菜单管理列表数据
     *
     * @return String
     */
    @GetMapping("/menu/list")
    @ResponseBody
    @ApiAuth(name = "菜单管理列表数据", permission = "admin:menu:list")
    public ResponseBean list(Menu menu) {
        List<Menu> list = menuService.list(menu);
        return ResponseBean.success("查询成功", list);
    }


    /**
     * 菜单添加页
     *
     * @return String
     */
    @RequestMapping("/menu/addPage/{pid}")
    @ApiAuth(name = "菜单添加页", permission = "admin:menu:addPage", type = Permission.ResType.NAV_LINK)
    public String addPage(@PathVariable("pid") String pid, Model model) {
        Menu menu = menuService.getById(pid);
        if (menu == null) {
            menu = new Menu();
            menu.setId("-1");
            menu.setType(0);
        }
        model.addAttribute("pMenu", menu);
        return view("static/admin/pages/menu/menu_add.html");
    }

    /**
     * 菜单编辑页
     *
     * @return String
     */
    @RequestMapping("/menu/editPage/{id}")
    @ApiAuth(name = "菜单编辑页", permission = "admin:menu:editPage", type = Permission.ResType.NAV_LINK)
    public String editPage(@PathVariable("id") String id, Model model) {
        Menu menu = menuService.getById(id);
        model.addAttribute("menu", menu);
        return view("static/admin/pages/menu/menu_edit.html");
    }

    /**
     * 添加菜单
     *
     * @return String
     */
    @PostMapping("/menu/add")
    @ResponseBody
    @ApiAuth(name = "添加菜单", permission = "admin:menu:add")
    public ResponseBean add(@RequestBody @Valid Menu menu) {
        if (menuService.add(menu) > 0) {
            String roles = menu.getRoles();
            if (StringUtil.isEmpty(roles)) {
                roles = Constants.ROLE_ADMIN;
            }
            menuService.updateMenuRole(menu, roles);
            return ResponseBean.success("添加成功", null);
        }
        logger.error("菜单添加失败: {}", menu.toString());
        return ResponseBean.fail("添加失败", null);
    }

    /**
     * 更新菜单
     *
     * @return String
     */
    @PostMapping("/menu/update")
    @ResponseBody
    @ApiAuth(name = "更新菜单", permission = "admin:menu:update")
    public ResponseBean update(@RequestBody @Valid Menu menu) {
        if (menuService.update(menu) > 0) {
            String roles = menu.getRoles();
            if (StringUtil.isEmpty(roles)) {
                roles = Constants.ROLE_ADMIN;
            }
            menuService.updateMenuRole(menu, roles);
            return ResponseBean.success("更新成功", null);
        }
        logger.error("菜单更新失败: {}", menu);
        return ResponseBean.fail("更新失败", null);
    }

    /**
     * 删除菜单
     *
     * @return String
     */
    @PostMapping("/menu/del")
    @ResponseBody
    @ApiAuth(name = "删除菜单", permission = "admin:menu:del")
    public ResponseBean del(@RequestBody String ids) {
        String[] idArr = ids.split(",");
        if (menuService.del(idArr) > 0) {
            return ResponseBean.success("删除成功", null);
        }
        logger.error("菜单删除失败: {}", ids);
        return ResponseBean.fail("删除失败", null);
    }

    /**
     * 更改状态
     *
     * @return String
     */
    @PostMapping("/menu/changeStatus")
    @ResponseBody
    @ApiAuth(name = "更改菜单状态", permission = "admin:menu:changeStatus")
    public ResponseBean changeStatus(@RequestBody Menu menu) {
        if (menuService.changeStatus(menu) > 0) {
            return ResponseBean.success("修改成功", null);
        }
        logger.error("菜单修改失败: {}", menu.toString());
        return ResponseBean.fail("修改失败", null);
    }

    /**
     * 获取菜单对应的角色
     *
     * @return String
     */
    @GetMapping("/menu/getRoles")
    @ResponseBody
    @ApiAuth(name = "获取菜单对应的角色", permission = "admin:menu:getMenuRoles")
    public ResponseBean getRoles(String id) {
        List<RoleMenuVO> roles = menuService.getMenuRolesByMenuId(id);
        return ResponseBean.success("获取成功", roles);
    }
}
