package com.dolphin.controller.admin;

import com.dolphin.base.BaseController;
import com.dolphin.commons.Constants;
import com.dolphin.commons.Pager;
import com.dolphin.commons.ResponseBean;
import com.dolphin.model.Permission;
import com.dolphin.model.Word;
import com.dolphin.permission.ApiAuth;
import com.dolphin.service.WordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 美句相关
 *
 * @author dolphin
 */
@Controller
@RequestMapping("/admin")
public class WordController extends BaseController {

    @Autowired
    private WordService wordService;


    /**
     * @return java.lang.String
     * @description 美句管理页
     * @author dolphin
     */
    @GetMapping("/word")
    @ApiAuth(name = "美句管理", groupId = Constants.ADMIN_MENU_GROUP_CONTENT, permission = "admin:word:index")
    public String index() {
        return "/static/admin/pages/word/list.html";
    }

    /**
     * @return java.lang.String
     * @description 添加美句
     * @author dolphin
     */
    @GetMapping("/word/addPage")
    @ApiAuth(name = "添加美句", type = Permission.ResType.NAV_LINK, permission = "admin:word:addPage")
    public String addCarouselPage() {
        return "/static/admin/pages/word/add.html";
    }

    /**
     * 编辑美句
     *
     * @return String
     */
    @GetMapping("/word/editPage/{id}")
    @ApiAuth(name = "编辑美句", type = Permission.ResType.NAV_LINK, permission = "admin:word:editPage")
    public String editPage(@PathVariable("id") String id, Model model) {
        Word word = wordService.getById(Long.valueOf(id));
        model.addAttribute("model", word);
        return view("/static/admin/pages/word/edit.html");
    }

    /**
     * @param pager pager
     * @return
     * @description 美句分页
     * @author dolphin
     */
    @PostMapping("/word/list")
    @ApiAuth(name = "美句分页", permission = "admin:word:list")
    @ResponseBody
    public Pager<Word> list(@RequestBody Pager<Word> pager) {

        return wordService.list(pager);
    }

    /**
     * 添加美句
     *
     * @return String
     */
    @PostMapping("/word/add")
    @ResponseBody
    @ApiAuth(name = "添加美句", permission = "admin:word:add")
    public ResponseBean add(@RequestBody @Valid Word word) {
        try {
            wordService.saveOrUpdate(word);
            return ResponseBean.success("添加成功", word);
        } catch (Exception e) {
            return ResponseBean.fail("添加失败", e.getMessage());
        }
    }

    /**
     * 删除美句
     *
     * @return String
     */
    @PostMapping("/word/del")
    @ResponseBody
    @ApiAuth(name = "删除美句", permission = "admin:word:del")
    public ResponseBean del(@RequestBody String id) {

        if (wordService.removeById(id)) {
            return ResponseBean.success("删除成功", null);
        }
        return ResponseBean.fail("删除失败", null);
    }

    /**
     * 更新美句
     *
     * @return String
     */
    @PostMapping("/word/update")
    @ResponseBody
    @ApiAuth(name = "更新美句", permission = "admin:word:update")
    public ResponseBean editcarousel(@RequestBody @Valid Word word) {
        if (wordService.updateById(word)) {
            return ResponseBean.success("更新成功", null);
        }
        return ResponseBean.fail("更新失败", null);
    }

}
