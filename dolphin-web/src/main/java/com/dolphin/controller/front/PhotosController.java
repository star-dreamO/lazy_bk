package com.dolphin.controller.front;

import com.dolphin.base.BaseController;
import com.dolphin.commons.FrontViewNodeRender;
import com.dolphin.service.PhotosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PhotosController extends BaseController {

    @Autowired
    private PhotosService photosService;

    @RequestMapping("/photos")
    @FrontViewNodeRender
    public String frontIndex(Model model) {
        model.addAttribute("url", "/photosList/");
        return view("/photos.html", "/photos.html", "static/admin/pages/photos/index.html");
    }

    @RequestMapping("/photosList/{pageIndex}")
    @FrontViewNodeRender
    public String articleListPage(@PathVariable("pageIndex") int pageIndex, Model model) {
        model.addAttribute("url", "/photosList/");
        model.addAttribute("pageIndex", pageIndex);
        return view("/photos.html", "/photos.html", "static/admin/pages/photos/index.html");
    }

    @RequestMapping("/photos/{id}")
    @FrontViewNodeRender
    public String frontPhotos(@PathVariable("id") Long id, Model model) {
        model.addAttribute("photos", photosService.getById(id));
        return view("/photoList.html", "/photoList.html", "static/admin/pages/photos/index.html");
    }

}
