package com.dolphin.controller.front;

import com.dolphin.base.BaseController;
import com.dolphin.commons.Constants;
import com.dolphin.commons.FrontViewNodeRender;
import com.dolphin.vo.ActicleSearchVO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ArticleSearchController extends BaseController {
    @RequestMapping("/article/search")
    @FrontViewNodeRender
    public String searchListPage(ActicleSearchVO acticleSearchVO, Model model) {
        model.addAttribute("url", Constants.URL_ARTICLE_SEARCH);
        model.addAttribute("title", acticleSearchVO.getTitle());
        model.addAttribute("categoryId", acticleSearchVO.getCategoryId());
        model.addAttribute("tagId", acticleSearchVO.getTagId());
        model.addAttribute("pageIndex", 1);
        return view(currentThemePage() + "/search.html");
    }

    @RequestMapping("/article/search/{pageIndex}")
    @FrontViewNodeRender
    public String searchListPage(ActicleSearchVO acticleSearchVO, @PathVariable("pageIndex") int pageIndex, Model model) {
        model.addAttribute("url", Constants.URL_ARTICLE_SEARCH);
        model.addAttribute("title", acticleSearchVO.getTitle());
        model.addAttribute("categoryId", acticleSearchVO.getCategoryId());
        model.addAttribute("tagId", acticleSearchVO.getTagId());
        model.addAttribute("pageIndex", pageIndex);
        return view(currentThemePage() + "/search.html");
    }
}
