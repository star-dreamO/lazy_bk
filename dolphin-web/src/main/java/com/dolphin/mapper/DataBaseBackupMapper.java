package com.dolphin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dolphin.model.DataBaseBackup;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author 林平
 * @create 2022-10-05 22:56
 * 个人博客地址：https://www.nonelonely.com
 */
@Mapper
@Component
public interface DataBaseBackupMapper extends BaseMapper<DataBaseBackup> {

    /**
     * 查询所有备份数据
     */
    List<DataBaseBackup> selectBackupsList();

    /**
     * 根据ID查询
     */
    DataBaseBackup selectListId(@Param("id") Long id);
}
