let util, table;
layui.use(['util', 'layer', 'table'], function () {
    util = layui.util;
    //layer = layui.layer;
    table = layui.table;

    queryTable();
    // 查询
    $("#queryBtn").click(function () {
        queryTable();
    });

    // 添加
    $("#addBtn").click(function () {
        layer.open({
            title: "添加定时任务",
            type: 2,
            area: common.layerArea($("html")[0].clientWidth, 500, 400),
            shadeClose: true,
            anim: 1,
            content: '/admin/scheduled/addPage'
        });
    });

});

/**
 * 查询表格数据
 */
function queryTable() {
    table.render({
        elem: '#tableBox',
        url: '/admin/scheduled/list',
        method: 'post',
        headers: {'Content-Type': 'application/json'},
        contentType: 'application/json',
        title: '定时任务列表',
        totalRow: false,
        limit: 10,
        where: {
            form: {
                name: $("#name").val()
            }
        },
        cols: [[
            {field: 'id', title: 'ID', width: 80, sort: true},
            {field: 'taskKey', minWidth: 160, title: 'bean名称'},
            {field: 'taskDesc', minWidth: 260, title: '任务描述'},
            {field: 'taskCron', minWidth: 260, title: '任务表达式'},
            {
                field: 'initStartFlag', minWidth: 160, title: '是否自动启动', templet: function (d) {
                    let str = "否"
                    if (d.initStartFlag == 1) {
                        str = "是"
                    }
                    return str;
                }
            },
            {field: 'startFlag', minWidth: 160, title: '运行状态' },
            {
                field: 'id', title: '操作', width: 200,
                templet: function (d) {
                    var html = "<div>" +
                        "<a class='layui-btn layui-btn-normal layui-btn-xs' onclick='editData(\""+d.id+"\")'>编辑</a> " +
                        "<a class='layui-btn layui-btn-danger layui-btn-xs' onclick='deleteData(\""+d.id+"\")'>删除</a>";
                    if (!d.startFlag) {
                        html = html + "<a class='layui-btn layui-btn-danger layui-btn-xs' onclick='start(\""+d.taskKey+"\")'>启动</a>";
                    } else {
                        html = html + "<a class='layui-btn layui-btn-danger layui-btn-xs' onclick='stop(\""+d.taskKey+"\")'>暂停</a>" ;
                    }
                    html = html + "</div>";
                    return html;
                }
            }
        ]],
        page: true,
        response: {statusCode: 200},
        parseData: function (res) {
            return {
                "code": res.code,
                "msg": res.msg,
                "count": res.total,
                "data": res.data
            };
        },
        request: {
            pageName: 'pageIndex',
            limitName: 'pageSize'
        }
    });
}

/**
 *
 * @param ids
 */
function deleteData(id) {
    layer.confirm('确定要删除吗?', {icon: 3, title: '提示'}, function (index) {
        $.ajax({
            type: "POST",
            url: "/admin/scheduled/del",
            contentType: "application/json",
            data: id,
            success: function (data) {
                if (data.code === 200) {
                    queryTable();
                    layer.msg(data.msg, {icon: 1});
                } else {
                    layer.msg(data.msg, {icon: 2});
                }
            },
            error: function (data) {
                layer.msg("删除失败", {icon: 2});
            }
        });
        layer.close(index);
    });
}

/**
 * 编辑
 * @param id
 */
function editData(id) {
    layer.open({
        title: "编辑定时任务",
        type: 2,
        area: common.layerArea($("html")[0].clientWidth, 500, 400),
        shadeClose: true,
        anim: 1,
        content: '/admin/scheduled/editPage/' + id
    });
}

/**
 * 暂停
 * @param id
 */
function stop(taskKey) {
    layer.confirm('确定要暂停吗?', {icon: 3, title: '提示'}, function (index) {
        $.ajax({
            type: "POST",
            url: "/admin/scheduled/stop",
            contentType: "application/json",
            data: taskKey,
            success: function (data) {
                if (data.code === 200) {
                    queryTable();
                    layer.msg(data.msg, {icon: 1});
                } else {
                    layer.msg(data.msg, {icon: 2});
                }
            },
            error: function (data) {
                layer.msg("删除失败", {icon: 2});
            }
        });
        layer.close(index);
    });
}

/**
 * 编辑
 * @param id
 */
function start(taskKey) {
    layer.confirm('确定要启动吗?', {icon: 3, title: '提示'}, function (index) {
        $.ajax({
            type: "POST",
            url: "/admin/scheduled/start",
            contentType: "application/json",
            data: taskKey,
            success: function (data) {
                if (data.code === 200) {
                    queryTable();
                    layer.msg(data.msg, {icon: 1});
                } else {
                    layer.msg(data.msg, {icon: 2});
                }
            },
            error: function (data) {
                layer.msg("删除失败", {icon: 2});
            }
        });
        layer.close(index);
    });
}

