(function ($) {
    let common = function () {
    }
    common.prototype = {
        formatDate: function (time, format) {
            let t = new Date(time);
            let tf = function (i) {
                return (i < 10 ? '0' : '') + i
            };
            return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function (a) {
                switch (a) {
                    case 'yyyy':
                        return tf(t.getFullYear());
                    case 'MM':
                        return tf(t.getMonth() + 1);
                    case 'mm':
                        return tf(t.getMinutes());
                    case 'dd':
                        return tf(t.getDate());
                    case 'HH':
                        return tf(t.getHours());
                    case 'ss':
                        return tf(t.getSeconds());
                }
            })
        },
        layerArea: function (clientWidth, width, height) {
            if (height !== 'auto') {
                height += 'px';
            }
            var a = clientWidth - width;
            if (a > 10) {
                return [width + 'px', height]
            } else {
                return ['100%', height]
            }
        }
    }
    window.common = new common();
})(window.jQuery);

window.Win10_parent = parent.Win10; //获取父级Win10对象的句柄
window.Win10_child = {
    close: function () {
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        Win10_parent._closeWin(index);
    },
    newMsg: function (title, content, handle_click) {
        Win10_parent.newMsg(title, content, handle_click)
    },
    openUrl: function (url, title, max) {
        var currentIndex = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        var click_lock_name = Math.random();
        Win10_parent._iframe_click_lock_children[click_lock_name] = true;
        var index = Win10_parent.openUrl(url, title, max, currentIndex);
        setTimeout(function () {
            delete Win10_parent._iframe_click_lock_children[click_lock_name];
        }, 1000);
        return index;
    }
};



